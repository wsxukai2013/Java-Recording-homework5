package test111;
public class ThreadTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DeadThread dt0=new DeadThread();
		DeadThread dt1=new DeadThread();
		Thread t0=new Thread(dt0);
		Thread t1=new Thread(dt1);
		dt0.flag=1;
		dt1.flag=0;
		t0.start();
		t1.start();
	}

}

class DeadThread implements Runnable {
	static Object o1 = new Object();
	static Object o2 = new Object();
	public int flag = 1;

	// public DeadThread(int flag) {
	// super();
	// this.flag = flag;
	// }

	public void run() {
		if (flag == 1) {
			while (true) 
			{
				synchronized (o1)
				{
					try {
						System.out.println("Thread name is:"+ Thread.currentThread().getName() + "..flag:"+ flag);
						Thread.sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					
					synchronized (o2) 
					{
						try {
							System.out.println("Thread name is:"+Thread.currentThread().getName()+ "..flag:" + flag);
							Thread.sleep(500);
						} 
						catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
		if (flag == 0) {
			while (true) {
				synchronized (o2) {
					try {
						System.out.println("Thread name is:"
								+ Thread.currentThread().getName() + "..flag:"
								+ flag);
						Thread.sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

					synchronized (o1) {
						try {
							System.out.println("Thread name is:"
									+ Thread.currentThread().getName()
									+ "..flag:" + flag);
							Thread.sleep(500);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
	}
 }